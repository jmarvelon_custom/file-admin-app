import { Injectable } from '@nestjs/common';
import { MailerService as Mailer } from '@nestjs-modules/mailer';
@Injectable()
export class MailerService {
  constructor(private readonly mailerService: Mailer) {}

  sendForgotPasswordEmail(to: string, password: string): Promise<void> {
    const subject = 'Recuperación de Contraseña';
    const text = `Hola,\n\nPara esta es tu nueva contraseña: ${password}\n\nSaludos`;

    return this.mailerService.sendMail({
      to,
      subject,
      text,
    });
  }
}
