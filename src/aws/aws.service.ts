import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { S3 } from 'aws-sdk';
import { AwsFileDto } from './dto/aws-file.dto';
import * as mimeTypes from 'mime-types';
import { UploadUrlDto } from 'src/file/dto/upload.dto';

@Injectable()
export class AwsService {
  private s3: S3;

  constructor(private readonly configService: ConfigService) {
    this.s3 = new S3({
      accessKeyId: this.configService.get('AWS_ACCESS_KEY_ID'),
      secretAccessKey: this.configService.get('AWS_SECRET_ACCESS_KEY'),
    });
  }

  async upload_file({ name, file }: AwsFileDto): Promise<string> {
    const base64data = Buffer.from(file.buffer, 'binary');
    const extension = mimeTypes.extension(file.mimetype);
    const key = `${this.configService.get('AWS_FOLDER')}/${name}.${extension}`;

    const params = {
      Bucket: this.configService.get('AWS_BUCKET'),
      Key: key,
      Body: base64data,
      ACL: 'public-read-write',
      ContentType: file.mimetype,
      ContentDisposition: 'inline',
      CreateBucketConfiguration: {
        LocationConstraint: 'ap-south-1',
      },
    };

    try {
      const data = await this.s3.upload(params).promise();
      return data.Location;
    } catch {
      throw new BadRequestException('Error uploading file to AWS S3');
    }
  }

  async upload_url({ name, url }: UploadUrlDto) {
    try {
      const image = await this.fetch_image(url);
      const base64data = Buffer.from(new Uint8Array(image));
      const path_url = new URL(url);
      const extension = path_url.pathname.includes('.')
        ? path_url.pathname.split('.').pop()
        : path_url.searchParams.get('fm');

      const key = `${this.configService.get('AWS_FOLDER')}/${name}.${extension}`;

      const params = {
        Bucket: this.configService.get('AWS_BUCKET'),
        Key: key,
        Body: base64data,
        ACL: 'public-read-write',
        ContentType: `image/${extension}`,
        ContentDisposition: 'inline',
        CreateBucketConfiguration: {
          LocationConstraint: 'ap-south-1',
        },
      };

      const data = await this.s3.upload(params).promise();
      return data.Location;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async download_file(file_url: string): Promise<{ file: any; key: string }> {
    const url = new URL(file_url);
    const path = url.pathname;
    const key = path.slice(1);

    const params = {
      Bucket: this.configService.get('AWS_BUCKET'),
      Key: key,
    };

    try {
      const data = this.s3.getObject(params).createReadStream();
      return { file: data, key: key };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async update_file(new_name: string, file_url: string) {
    try {
      const url = new URL(file_url);
      const path = url.pathname;

      const old_key = path.slice(1);
      const extension = old_key.split('.')[1];

      const copy = `${this.configService.get('AWS_BUCKET')}/${old_key}`;
      const new_key = `${this.configService.get('AWS_FOLDER')}/${new_name}.${extension}`;

      const params = {
        Bucket: this.configService.get('AWS_BUCKET'),
        CopySource: copy,
        Key: new_key,
        ACL: 'public-read-write',
      };

      const updated_file = await this.s3.copyObject(params).promise();
      if (!updated_file)
        throw new BadRequestException('File was not in AWS S3');

      const new_url = `https://${this.configService.get('AWS_BUCKET')}.s3.amazonaws.com/${new_key}`;

      return new_url;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async remove_file(file_url: string) {
    try {
      const url = new URL(file_url);
      const path = url.pathname;
      const key = path.slice(1);

      const params = {
        Bucket: this.configService.get('AWS_BUCKET'),
        Key: key,
      };

      const removed_file = await this.s3.deleteObject(params).promise();

      return removed_file;
    } catch {
      throw new BadRequestException('Error deleting file from AWS S3');
    }
  }

  private async fetch_image(url: string) {
    try {
      const response = await fetch(url);
      if (!response.ok)
        throw new BadRequestException(
          `Failed to fetch image: ${response.statusText}`,
        );

      return response.arrayBuffer();
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
