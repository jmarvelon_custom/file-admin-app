import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { RegisterUserDto } from './dto/register-user.dto';
import { LoginUserDto } from './dto/login-auth.dto';
import { Model } from 'mongoose';
import { User } from './entities/user.entity';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { MailerService } from 'src/mailer/mailer.service';
import { JwtInterface } from './interfaces/jwt.interface';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    private readonly mailerService: MailerService,
    private readonly jwtService: JwtService,
  ) {}

  async create(registerUser: RegisterUserDto) {
    try {
      //Data transformation email, password
      registerUser.email = registerUser.email.toLowerCase().trim();

      const hashPassword = await bcrypt.hashSync(registerUser.password, 10);
      registerUser.password = hashPassword;

      //User creation
      const user = await this.userModel.create({ ...registerUser });

      const { password: _p, __v: _v, ...response } = user.toJSON();

      const token = this.getJwtToken(response);

      return { ...response, token };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async login({ email, password }: LoginUserDto) {
    try {
      //Find user by email
      const user = await this.userModel.findOne({ email });

      if (!user) throw new UnauthorizedException('User is not found');

      //Compare password with bcrypt
      if (!bcrypt.compareSync(password, user.password))
        throw new UnauthorizedException('Password invalid');

      const { password: _p, __v: _v, ...response } = user.toJSON();

      const token = this.getJwtToken(response);

      return { ...response, token };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async forgot_password({ email }: ForgotPasswordDto) {
    try {
      //Find user by email
      const user = await this.userModel.findOne({ email });

      if (!user) throw new UnauthorizedException('User is not found');

      const newPassword = this.generateRandomPassword();
      const newHash = bcrypt.hashSync(newPassword, 10);

      //Update new password on user document
      const update = await this.userModel.findByIdAndUpdate(user._id, {
        password: newHash,
      });

      if (!update)
        throw new BadRequestException('New password could not be generated');

      const mail = this.mailerService.sendForgotPasswordEmail(
        user.email,
        newPassword,
      );

      if (!mail) throw new BadRequestException('Email was not sended');

      return { message: 'New password was send to your email' };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  private getJwtToken(payload: JwtInterface) {
    const token = this.jwtService.sign(payload);

    return token;
  }

  private generateRandomPassword(): string {
    const numbers = '0123456789';
    const uppercaseLetters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
    const symbols = '!@#$%^&*()-_=+[{]}|;:,<.>/?';

    let password = '';

    // Generate UpperCase Letter
    password += uppercaseLetters.charAt(
      Math.floor(Math.random() * uppercaseLetters.length),
    );

    // Generate LowerCase Letter
    password += lowercaseLetters.charAt(
      Math.floor(Math.random() * lowercaseLetters.length),
    );

    // Generate Symbol
    password += symbols.charAt(Math.floor(Math.random() * symbols.length));

    // Generate numeric string
    for (let i = 0; i < 5; i++) {
      password += numbers.charAt(Math.floor(Math.random() * numbers.length));
    }

    // Mix characters
    password = password
      .split('')
      .sort(() => Math.random() - 0.5)
      .join('');

    return password;
  }
}
