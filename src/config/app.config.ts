export const EnviromentConfig = () => ({
  port: process.env.PORT || 3000,
  dbUrl: process.env.DB_URL,
  jwt: process.env.JWT_SECRET,
});
