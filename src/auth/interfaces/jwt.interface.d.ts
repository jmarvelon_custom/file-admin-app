export interface JwtInterface {
  _id: string;
  name: string;
  lastname: string;
  email: string;
}
