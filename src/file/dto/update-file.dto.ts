import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class UpdateFileDto {
  @ApiProperty({
    example: 'File Name',
    required: true,
  })
  @IsString()
  @MinLength(4)
  @IsNotEmpty()
  name: string;
}
