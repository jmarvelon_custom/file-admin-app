import { BadRequestException, Injectable } from '@nestjs/common';
import { UploadFileDto, UploadUrlDto } from './dto/upload.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { AwsService } from 'src/aws/aws.service';
import { File } from './entities/file.entity';
import { SearchService } from 'src/search/search.service';

@Injectable()
export class FileService {
  constructor(
    @InjectModel(File.name)
    private readonly fileModel: Model<File>,
    private readonly awsService: AwsService,
    private readonly searchService: SearchService,
  ) {}

  async upload_file({ name }: UploadFileDto, buffer: Express.Multer.File) {
    try {
      const url = await this.awsService.upload_file({ name, file: buffer });
      if (!url) throw new BadRequestException('File was not upload');

      const file = await this.fileModel.create({
        name: name,
        url,
      });

      if (!file) throw new BadRequestException('File was not created');

      return file;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async upload_url(uploadUrlDto: UploadUrlDto) {
    try {
      const url = await this.awsService.upload_url(uploadUrlDto);
      if (!url) throw new BadRequestException('File was not upload');

      const file = await this.fileModel.create({
        name: uploadUrlDto.name,
        url,
      });

      if (!file) throw new BadRequestException('File was not created');

      return file;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async upload_random() {
    try {
      const random_unsplash = await this.searchService.findRandom();
      const url = await this.awsService.upload_url({
        name: random_unsplash.slug,
        url: random_unsplash.urls.regular,
      });
      if (!url) throw new BadRequestException('File was not upload');

      const file = await this.fileModel.create({
        name: random_unsplash.slug,
        url,
      });

      if (!file) throw new BadRequestException('File was not created');

      return file;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async findAll() {
    try {
      const files = await this.fileModel.find();
      if (!files) throw new BadRequestException('Files were not found');
      return files;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async findOne(id: string) {
    try {
      const file = await this.fileModel.findById(id);
      if (!file) throw new BadRequestException('File was not found');
      return file;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async downloadOne(id: string) {
    try {
      const file = await this.fileModel.findById(id);
      if (!file) throw new BadRequestException('File was not found');

      const buffer = await this.awsService.download_file(file.url);
      if (!file) throw new BadRequestException('Object was not download');

      return buffer;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async update(id: string, { name }: UpdateFileDto) {
    try {
      const file = await this.fileModel.findById(id);
      if (!file) throw new BadRequestException('File was not found');

      const update_file_aws = await this.awsService.update_file(name, file.url);

      // This line able to delete file from aws s3, however there's not permissions for.
      // const _remove_file_aws = await this.awsService.remove_file(file.url);

      const updated_file = await this.fileModel.findByIdAndUpdate(
        id,
        {
          name,
          url: update_file_aws,
        },
        { new: true },
      );
      if (!updated_file) throw new BadRequestException('File was not updated');

      return updated_file;
    } catch (error) {
      throw new Error(error);
    }
  }

  async remove(id: string) {
    try {
      const file = await this.fileModel.findById(id);
      if (!file) throw new BadRequestException('File was not found');

      const remove_file = await this.fileModel.findByIdAndDelete(id);
      if (!remove_file) throw new BadRequestException('File was not removed');

      // This line able to delete file from aws s3, however there's not permissions for.
      // const _remove_file_aws = await this.awsService.remove_file(file.url);

      return { message: 'File was deleted successfully', code: 200 };
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
