import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { SearchService } from './search.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Search Module (Power by Unsplash API)')
@Controller('search')
@UseGuards(AuthGuard())
export class SearchController {
  constructor(private readonly searchService: SearchService) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Images found.',
  })
  @ApiResponse({ status: 400, description: 'Images were not found' })
  findOne(@Query('query') query: string) {
    return this.searchService.findQuery({ query });
  }
}
