import { Module } from '@nestjs/common';
import { SearchService } from './search.service';
import { SearchController } from './search.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  controllers: [SearchController],
  providers: [SearchService, ConfigService],
  imports: [ConfigModule, AuthModule],
})
export class SearchModule {}
