import { BadRequestException, Injectable } from '@nestjs/common';
import { SearchDto } from './dto/search.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class SearchService {
  constructor(private readonly configService: ConfigService) {}

  headers = {
    Authorization: `Client-ID ${this.configService.get('UNSPLASH_KEY')}`,
    'Content-Type': 'application/json',
  };

  async findQuery({ query }: SearchDto) {
    try {
      const url = `${this.configService.get('UNSPLASH_HOST')}/search/photos?query=${query}`;

      const response = await fetch(url, {
        method: 'GET',
        headers: this.headers,
      });
      if (!response.ok)
        throw new BadRequestException(`Error looking for a ${query} image`);

      const result = response.json();

      return result;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async findRandom() {
    try {
      const url = `${this.configService.get('UNSPLASH_HOST')}/photos/random`;

      const response = await fetch(url, {
        method: 'GET',
        headers: this.headers,
      });
      if (!response.ok)
        throw new BadRequestException(`Error looking for a random image`);

      const result = response.json();

      return result;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
