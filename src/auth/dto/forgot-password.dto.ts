import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString } from 'class-validator';

export class ForgotPasswordDto {
  @ApiProperty({
    example: 'example@gmail.com',
    required: true,
  })
  @IsString()
  @IsEmail()
  email: string;
}
