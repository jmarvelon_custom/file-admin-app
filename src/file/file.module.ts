import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { FileController } from './file.controller';
import { AuthModule } from 'src/auth/auth.module';
import { AwsService } from 'src/aws/aws.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AwsModule } from 'src/aws/aws.module';
import { MongooseModule } from '@nestjs/mongoose';
import { File, FileSchema } from './entities/file.entity';
import { SearchModule } from 'src/search/search.module';
import { SearchService } from 'src/search/search.service';

@Module({
  controllers: [FileController],
  providers: [FileService, AwsService, ConfigService, SearchService],
  imports: [
    MongooseModule.forFeature([{ name: File.name, schema: FileSchema }]),
    AuthModule,
    AwsModule,
    ConfigModule,
    SearchModule,
  ],
})
export class FileModule {}
