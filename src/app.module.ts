import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { MailerModule } from './mailer/mailer.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EnviromentConfig } from './config/app.config';
import { FileModule } from './file/file.module';
import { AwsModule } from './aws/aws.module';
import { SearchModule } from './search/search.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [EnviromentConfig],
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule], // Import ConfigModule here
      inject: [ConfigService], // Inject ConfigService here
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get('dbUrl'),
      }),
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    AuthModule,
    MailerModule,
    FileModule,
    AwsModule,
    SearchModule,
  ],
})
export class AppModule {}
