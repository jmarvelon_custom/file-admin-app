import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Res,
} from '@nestjs/common';
import { FileService } from './file.service';
import { UploadFileDto, UploadUrlDto } from './dto/upload.dto';
import { UpdateFileDto } from './dto/update-file.dto';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Files Module')
@Controller('file')
@UseGuards(AuthGuard())
export class FileController {
  constructor(private readonly fileService: FileService) {}

  @Post('upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'File to load',
    type: UploadFileDto,
  })
  @ApiResponse({
    status: 201,
    description: 'File has been successfully uploaded.',
  })
  @ApiResponse({ status: 400, description: 'File was not uploaded.' })
  @UseInterceptors(FileInterceptor('file'))
  upload(
    @UploadedFile() file: Express.Multer.File,
    @Body() createFileDto: UploadFileDto,
  ) {
    return this.fileService.upload_file(createFileDto, file);
  }

  @Post('upload/url')
  @ApiResponse({
    status: 201,
    description: 'Image has been successfully uploaded.',
  })
  @ApiResponse({ status: 400, description: 'Image was not uploaded.' })
  upload_url(@Body() createFileDto: UploadUrlDto) {
    return this.fileService.upload_url(createFileDto);
  }

  @Get('upload/random')
  @ApiResponse({
    status: 201,
    description: 'Image has been successfully uploaded.',
  })
  @ApiResponse({ status: 400, description: 'Image was not uploaded.' })
  upload_random() {
    return this.fileService.upload_random();
  }

  @Get('download/:id')
  @ApiResponse({
    status: 200,
    description: 'Image found.',
  })
  @ApiResponse({ status: 400, description: 'Image was not found.' })
  async downloadOne(@Param('id') id: string, @Res() res: Response) {
    const data = await this.fileService.downloadOne(id);
    return data.file.pipe(res);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Images found.',
  })
  @ApiResponse({ status: 400, description: 'Images were not found.' })
  findAll() {
    return this.fileService.findAll();
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'Image found.',
  })
  @ApiResponse({ status: 400, description: 'Image was not found.' })
  findOne(@Param('id') id: string) {
    return this.fileService.findOne(id);
  }

  @Patch(':id')
  @ApiResponse({
    status: 200,
    description: 'File was updated successfully.',
  })
  @ApiResponse({ status: 400, description: 'File was not updated.' })
  update(@Param('id') id: string, @Body() updateFileDto: UpdateFileDto) {
    return this.fileService.update(id, updateFileDto);
  }

  @Delete(':id')
  @ApiResponse({
    status: 200,
    description: 'File was deleted.',
  })
  @ApiResponse({ status: 400, description: 'File was not deleted.' })
  remove(@Param('id') id: string) {
    return this.fileService.remove(id);
  }
}
