import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUrl, MinLength } from 'class-validator';

export class UploadFileDto {
  @ApiProperty({
    example: 'File Name',
    required: true,
  })
  @IsString()
  @MinLength(4)
  @IsNotEmpty()
  name: string;
}

export class UploadUrlDto {
  @ApiProperty({
    example: 'File Name',
    required: true,
  })
  @IsString()
  @MinLength(4)
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    example: 'File Url',
    required: true,
  })
  @IsString()
  @IsUrl()
  @IsNotEmpty()
  url: string;
}
