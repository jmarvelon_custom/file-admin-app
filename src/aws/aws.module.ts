import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { AwsService } from './aws.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  providers: [AwsService, ConfigService],
  imports: [
    ConfigModule,
    MulterModule.register({
      dest: './uploads', // folder to save file upload
    }),
  ],
})
export class AwsModule {}
