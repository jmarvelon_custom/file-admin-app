import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsString,
  IsStrongPassword,
  MinLength,
} from 'class-validator';

export class RegisterUserDto {
  @ApiProperty({
    example: 'Carlos',
    required: true,
  })
  @IsString()
  @MinLength(2)
  name: string;

  @ApiProperty({
    example: 'Lopez',
    required: true,
  })
  @IsString()
  @MinLength(2)
  lastname: string;

  @ApiProperty({
    example: 'example@gmail.com',
    required: true,
  })
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty({
    example: '12345678Ab*',
    required: true,
  })
  @IsString()
  @IsStrongPassword({
    minLength: 6,
    minUppercase: 1,
    minNumbers: 1,
    minSymbols: 1,
  })
  password: string;
}
